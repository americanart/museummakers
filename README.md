
# Museum Makers Guide
[Visit the Museum Makers Guide](https://museummakers.guide/)

The Museum Makers Guide is a static site built with [Vuepress](https://vuepress.vuejs.org/guide/).
The guide is a collaborative document for DIYers, educators, and museum professionals who are using maker 
technologies in museums.

## Contributing
See the [contributing guide](https://museummakers.guide/contributing.html) for more information about 
adding or editing content, or below for more information about cloning and working with the project locally.

## Building the project locally
Clone this repository to a local directory.
This project requires Node.js and uses [yarn](https://yarnpkg.com), you'll need to install this globally before you can 
get started.

```
npm install -g yarn
```

Then you need to install the project dependencies:

```
yarn install
```

Now you're ready to go.
To run the local dev server just use the following command:

```
yarn start
```

Your website should be available at [http://localhost:8080]



