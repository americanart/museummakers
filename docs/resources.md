---
title: Resources and Links
lang: en-US
meta:
  - name: description
    content: Additional information, resources, and links to help makers
---
# Resources

Links to over useful guides, projects, and resources.

## Documentation
- [Raspberry Pi Docs](https://www.raspberrypi.org/documentation/)

## Projects
Links to other projects that are not in this guide but may provide inspiration for other uses.

#### [Cooper Hewitt Pen](https://www.cooperhewitt.org/new-experience/designing-pen/)
A NFC reader in a "pen" device that museum visitor's used to read (and later retrieve) information from object labels.

#### [Lost Map of Neverland](https://mw19.mwconf.org/paper/end-to-end-experience-design-lessons-for-all-from-the-nfc-enhanced-lost-map-of-wonderland%e2%80%8a-2/)
A project using NFC Tags embedded in a paper map which was given to visitors to create interactive experiences:
<br>
<iframe src="https://player.vimeo.com/video/312429937" width="100%" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    
#### [Large Display Controlled by SMS](https://www.hackster.io/dj-lukas/large-display-controlled-by-sms-33d47a)
Display made of LED strip. Displays the number sent by SMS. Arduino Pro Mini + SIM800
