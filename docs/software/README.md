---
title: Software
lang: en-US
meta:
  - name: description
    content: Information about operating systems, scripts, and libraries that can be used in DIY Maker projects
---
# Software

Programming languages, scripts, and libraries. This is code that you use to tie things together and start making interactions.

## Libraries & Packages

### Pi Presents
A Multi-media Toolkit for Museums, Visitor Centres and more running on [Raspberry Pi](/hardware/raspberry-pi.md)
<br>
[Read more](/software/libraries/pi-presents.md) 

### OMXPlayer
A command line media player that works on [Raspbian](/software/raspbian.md)
<br>
[Read more](/software/libraries/omxplayer.md) 

## Scripts

### Tangible Interface Script
A Python script used in the [Hello World](/projects/hello-world.md) project to play a different video based GPIO 
(General-Purpose Input/Output) Pins.
<br>
[The Git Repository](https://github.com/vnc-ncsu/simpletangible_prj_001/blob/master/files/tangibleScript.py)

### Tune Player Script
A Node.js script used to play a video from a Raspberry Pi, and send an HTTP Request to a configurable remote server when
the specific (looped) video finishes playing and begins playing from the beginning.
<br>
[Read more](/software/tune-player-script.md)

## Operating Systems

### [Raspbian](/software/raspbian.md) 
Linux OS used with [Raspberry Pi](/hardware/raspberry-pi.md).

### [Pidora](/software/pidora.md) 
Linux OS based on Fedora for [Raspberry Pi](/hardware/raspberry-pi.md). One of Pidora's most talked about features is the
ability to be [set up in "headless" mode](https://github.com/ksylvan/RaspberryPiSetup/blob/master/README.md), allowing you
to install on a Pi without a monitor.

## Proprietary Software
While not necessarily a "maker" technology these applications are common in museums, and things you make will often have to 
interface with them.

### Medialon
A Windows based application for managing and controlling media playback and lighting
<br>
[Read more](/software/proprietary/medialon.md)

### BrightSign
A suite of applications (and a media players) used for digital signage
<br>
[Read more](/software/proprietary/brightsign.md)
