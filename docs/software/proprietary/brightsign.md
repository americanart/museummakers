---
title: BrightSign
lang: en-US
meta:
  - name: description
    content: A suite of applications (and a media players) used for digital signage
  - name: keywords
    content: Digital Signage
---

# BrightSign

## Links
- [Company website](https://www.brightsign.biz/)
- BrightSign [HTML Development Documentation](https://docs.brightsign.biz/display/DOC/HTML+Development)
- BrightSign [REST API Documentation](https://docs.brightsign.biz/display/DOC/REST+API)

TODO: info, links, a specfic scripts to interface with BrightSign, BrightAuthor, etc.