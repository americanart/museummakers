---
title: Raspbian
lang: en-US
meta:
  - name: description
    content: Information to get people started with the Raspbian
---
# Raspbian

A common Linux distribution used as the Operating System for [Raspberry Pi](/hardware/raspberry-pi.md).

```
TODO: information to get people started with Raspbian.
```
