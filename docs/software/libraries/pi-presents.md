---
title: Pi Presents
lang: en-US
meta:
  - name: description
    content: A Multi-media Toolkit for Museums, Visitor Centres and more running on the Raspberry Pi
---

# Pi Presents

## Links

- [Git repository](https://github.com/KenT2/pipresents-gapless)
- [PDF Manual](https://github.com/KenT2/pipresents-gapless/blob/master/manual.pdf)

## Description

A (Python) toolkit installed on a Raspberry Pi for producing interactive multimedia applications for museums. The 
toolkit can be configured without programming knowledge. 

Pi Presents could be extended to run many types of applications but the [website](https://pipresents.wordpress.com/) lists 
the following applications as general examples:

- Animation or interpretation of museum exhibits by triggering sounds, video, or a slideshow from a button, keyboard, or 
GPIO inputs.
- While playing media GPIO outputs can be used to control external devices such as lights, moving machinery, 
animatronics etc.
- Multi-media shows for  visitor centre. Images, videos, audio tracks, and messages can be displayed. Repeats can be at 
intervals or at specified times of day.
- Allow media shows to be interrupted by the visitor and a menu of alternative shows to be presented.
- Giving presentations where progress through slides is manually controlled by buttons or keyboard. The presentation may 
include images, text, audio tracks and videos.
- Using Liveshow you can dynamically upload image, video or audio tracks over the network for display in a repeating show.
- Run multiple shows simultaneously so that more than one exhibit can be controlled from one Pi or complex interactive 
exhibits can be controlled.

## Demo
<iframe width="100%" height="315" src="https://www.youtube.com/embed/yEGxdEQc8Dk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
