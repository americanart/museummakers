---
title: Tangible Interfaces "Hello World" Project
lang: en-US
meta:
  - name: description
    content: Hello World tutorial using Raspberry Pi and other maker technologies
  - name: keywords
    content: Tangible Interfaces Tutorial
---

# Tangible Interfaces "Hello World"

This project was presented by Todd Berreth at the [2019 Museums and the Web conference](https://mw19.mwconf.org/proposal/a-tangible-interaction-with-trajans-weapons-frieze-an-open-source-model-for-exhibition-development-and-hands-on-storytelling/).
An outline of the project is given here, but the [complete description](https://simpletangible.wordpress.ncsu.edu/tutorial/), [PDF tutorial](http://simpletangible.wordpress.ncsu.edu/files/2019/02/SimpleTangible_prg_001_tutorial.pdf), 
and a [code repository with project files](https://github.com/vnc-ncsu/simpletangible_prj_001) have been provided by the project authors.


## Goals

![Image of the Hello World Interface box and screen](/assets/img/projects/hello-world/goals.png)
Build a box with different shaped pegs. Placing a peg in the box controls a video that is displayed on screen.
This project serves as simple introduction to creating [tangible interfaces](https://en.wikipedia.org/wiki/Tangible_user_interface),
and provides a solid introduction to some of the common tools and materials used.

## Tools & Materials

- [Raspberry Pi](/hardware/raspberry-pi.md) 3 Model B
- Micro SD card
- 5V 2.4A Power Supply
- Jumper wires

# Scripts
- [Tangible Script](/software/#tangible-interface-script)

## Notes
The polished final project requires:
- Access to a laser cutting machine for cutting the box pattern from a wood sheet. 
- Access to a 3-D Printer for the blocks.

## Links

- [The printable tutorial](http://simpletangible.wordpress.ncsu.edu/files/2019/02/SimpleTangible_prg_001_tutorial.pdf)

## Similar
- [Museum in a Box](http://www.museuminabox.org)

:tada:

