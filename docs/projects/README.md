---
title: Projects
lang: en-US
meta:
  - name: description
    content: Information about specific DIY Maker projects for museums
---
# Projects & Tutorials

This document is primarily organized around these standalone projects. 

## Tangible Interfaces "Hello World"
A good place to start for beginners. Like programming "Hello World" set ups, this project includes a detailed tutorial
to get you started building tangible user interfaces, and requires getting familiar with SBC (single board computers), scripting,
and fabrication technologies.
<br>
[Read more](/projects/hello-world.md)

## Museums in a Box
Museums in a box is a starter kit available to order, comprised of 3D prints, postcards, 
near-field communication (NFC) technology, and a Raspberry Pi. The goal of this project is to provide teachers, students,
and museum staff with the tools necessary to get started with maker technologies.
<br>
[Read more](http://www.museuminabox.org)
