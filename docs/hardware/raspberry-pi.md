---
title: Raspberry Pi
lang: en-US
meta:
  - name: description
    content: Information to get people started using the Raspberry Pi in museums
  - name: keywords
    content: Raspberry Pi
---
# Raspberry Pi

TODO: Information to get people started with Pi

## Operating Systems
- [Raspbian](/software/raspbian.md)