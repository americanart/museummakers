---
title: Hardware
lang: en-US
meta:
  - name: description
    content: Information about hardware used in DIY Maker projects for museums
---
# Hardware

Hardware are Single Board Computers, Controllers, storage cards, cables, etc.

## Raspberry Pi
A tiny and affordable (and fun!) computer that you can used by museum makers.
<br>
[Read more](/hardware/raspberry-pi.md)

## Pi Touch
TODO: more info