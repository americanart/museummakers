---
title: Material
lang: en-US
meta:
  - name: description
    content: Information about materials and fabrication tools common to maker projects for museum exhibitions
---
# Material

You've got Raspberry Pi's and python scripting? How about laser cutters, 3-D printers, hammers, and nails? This is where 
you go to Home Depot and start making something "material".

## 3D Printing
3D Printing is the process of creating a three-dimensional object, with material being added together, typically layer by layer.
<br>
[Read more](/material/3d-printing/README.md)

## Laser Cutting
Laser cutting is a technology that uses a laser to cut materials.
<br>
[Read more](/material/laser-cutting/README.md)

TODO: Continue adding content for common materials/fabrication tools.
