---
title: 3D Printing
lang: en-US
meta:
  - name: description
    content: A guide to get you started using 3D Printing for museum projects
---
# 3D Printing

## About

TODO: Build out content about printing and acquiring or getting access to a printer

## Models
TODO: Some example models, instructions, and other places to find models
### Tangible Shapes
The [Hello World](/projects/hello-world.md) project provides models for some simple shapes (hex, cylinder, cube)
in a [tangibles.zip file on Github](https://github.com/vnc-ncsu/simpletangible_prj_001/blob/master/files/tangibles.zip).