module.exports = {
    title: 'Museum Makers Guide',
    description: 'A collaborative document for DIYers, educators, and museum professionals who are using maker technologies in museums.',
    base: '/',
    dest: 'public',
    head: [
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['meta', { name: 'theme-color', content: '#000000' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: '#000000' }],
        ['link', { rel: 'apple-touch-icon', href: `/assets/img/icons/apple-touch-icon.png` }]
    ],
    themeConfig: {
        sidebar: [
            '/',
            ['/projects/', 'Projects & Tutorials'],
            ['/hardware/', 'Hardware'],
            ['/software/', 'Software'],
            ['/material/', 'Material'],
            ['/resources', 'Resources'],
            ['/contributing', 'Contribution guide']
        ],
        sidebarDepth: 2,
        displayAllHeaders: true,
        lastUpdated: 'Updated',
        repo: 'https://gitlab.com/americanart/museummakers',
        repoLabel: 'Contribute',
        editLinks: true,
        docsDir: 'docs',
        docsBranch: 'draft',
        editLinkText: 'Edit this page.',
    }
}