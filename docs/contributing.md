---
title: Contribute
lang: en-US
meta:
  - name: description
    content: Instructions for contributing to the museum makers guide
  - name: keywords
    content: open source
---

# Contributing

Anyone with a [GitLab account](https://gitlab.com/users/sign_in) (You can create an account with your Github account, btw :smiley:) 
can contribute to this guide using the [GitLab Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) in a browser. 
Please fork the project and make changes on your fork (see [forking workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html) instructions) 
commit those changes and submit a merge request to the `draft` branch of the upstream repository. A maintainer will
review your changes merge them into the `master` branch. The static site is rebuilt when a merge request is accepted.

The site content resides in markdown files in the `/docs` directory. Navigation is based on the following directory 
structure. The `README.md` file always acts as the "index" file for a directory. When creating a markdown file under one 
of these directories update the README.md file with a section description and "Read more" link to the new page.
```
/docs
    |-- material
        -- plastics.md // An example page about the material "plastics" 
        -- README.md // The index page for all "materials".
    |-- projects
        -- my-project.md // An example project page
        -- README.md // The index page for all "projects".
    |-- hardware
        -- wires.md // An example page about "wires"
        -- README.md // The index page for all "hardware".
    |-- software
        -- raspbian.md // An example page about "raspbian" OS
        -- README.md // The index page for all "software".
```

Local images are added to `/docs/.vuepress/public/assets/img` and files to `/docs/.vuepress/public/assets/files`

In addition, basic page metadata can be added to a markdown file by adding a [front matter](https://vuepress.vuejs.org/guide/markdown.html#front-matter) 
block to the top of the file.
<br> 
For example:

```yaml
---
title: This is the page title
lang: en-US
meta:
  - name: description
    content: The content of the "description" metatag
  - name: keywords
    content: The content of the "keywords" metatag
---
```

See the [GitLab markdown guide](https://docs.gitlab.com/ee/user/markdown.html) for general information about Markdown 
formatting or the [Vuepress markdown guide](https://v0.vuepress.vuejs.org/guide/markdown.html#github-style-tables) for some specifics if you are already familiar. 
(You can also test in this [online markdown editor](https://markdown-it.github.io/).)

Additionally, you can ask questions or make suggestions by [creating an issue](https://gitlab.com/americanart/museummakers/issues).

:tada: "Happy Making! :tada: