---
home: true
heroImage: /assets/img/icons/icon-512x512.png
actionText: Get Started →
actionLink: /projects/
features:
- title: DIY Projects as "chapters"
  details: This document is primarily organized around specific "how to" projects. Specific projects might link to other pages with more information about types of hardware, software, are building materials used.
- title: Hardware/Software
  details: Single board computers, RFID, and other hardware, and the software/scripts that make things "talk". Detail pages provide general information and link back to project implementations.
- title: Material
  details: Details, materials, tips, and tricks for building non-electonic projects. The hardest part about building a talking chair might be building the chair. (Also, please someone build a talking chair!)
---

This document is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/) License. <!--{.center}-->

